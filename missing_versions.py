import sys
import json

from distutils.version import StrictVersion
from optparse import OptionParser
from urllib.request import urlopen, Request


def versions_pypi(package, min_version):
    url = 'https://pypi.org/pypi/{package}/json'.format(package=package)
    data = json.load(urlopen(Request(url)))
    versions = data.get('releases').keys()
    if min_version is not None:
        strict_min = StrictVersion(min_version)
    else:
        strict_min = None
    valid_versions = []
    for version in versions:
        try:
            strict_version = StrictVersion(version)
            if strict_version.prerelease is not None:
                continue
            if strict_min is None or strict_version >= strict_min:
                valid_versions.append(version)
        except:
            continue
    valid_versions.sort(key=StrictVersion)
    return valid_versions


def versions_registry(project_id):
    url = 'https://gitlab.com/api/v4/projects/{project_id}/registry/repositories?tags=true'.format(project_id=project_id)
    data = json.load(urlopen(Request(url)))
    versions = []
    for item in data:
        if 'tags' in item:
            for tag in item['tags']:
                try:
                    version = tag.get('name')
                    strict_version = StrictVersion(version)
                    if strict_version.prerelease is not None or version in versions:
                        continue
                    versions.append(version)
                except:
                    continue
    return versions


def versions_missing(package, min_version, project_id):
    pypi = versions_pypi(package, min_version)
    registry = versions_registry(project_id)
    missing = []
    for version in pypi:
        if version not in registry:
            missing.append(version)
    missing.sort(key=StrictVersion)
    return missing


parser = OptionParser()
parser.add_option(
    '-p', '--package',
    dest='package',
    type='string',
    metavar='PACKAGE',
    help='Name of the package on PyPI'
)
parser.add_option(
    '-r', '--project-id',
    dest='project_id',
    type='string',
    metavar='PROJECT ID',
    help='ID of the GitLab project'
)
parser.add_option(
    '-m', '--min-version',
    dest='min_version',
    type='string',
    metavar='MINIMUM VERSION',
    help='The lowest version to be returned.'
)
parser.add_option(
    '-a', '--all',
    dest='all',
    action='store_true',
    help='Return all versions from PyPI',
    default=False
)
parser.add_option(
    '-l', '--latest',
    dest='latest',
    action='store_true',
    help='Return only latest version from PyPI',
    default=False
)
(options, args) = parser.parse_args();

if not len(options.package) > 0:
    raise ValueError('No package defined. Use --package option.')

if not len(options.project_id) > 0:
    raise ValueError('No project_id defined. Use --project-id option.')

if options.latest:
    print(versions_pypi(options.package, options.min_version)[-1])
elif options.all:
    print('\n'.join(versions_pypi(options.package, options.min_version)))
else:
    print('\n'.join(versions_missing(options.package, options.min_version, options.project_id)))

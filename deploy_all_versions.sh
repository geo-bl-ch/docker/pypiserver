
set -e

VERSIONS=$(python3 missing_versions.py --package=pypiserver --project-id=$CI_PROJECT_ID --all --min-version=1.0.0)

for version in $VERSIONS
do
    sh deploy_version.sh $version
done
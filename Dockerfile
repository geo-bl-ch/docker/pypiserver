FROM alpine:3.11

LABEL maintainer="Karsten Deininger <karsten.deininger@bl.ch>"

ARG version=1.3.1

ENV LANG="C.UTF-8"

# Install dependencies
RUN apk --no-cache add \
        bash \
        python3 \
        py3-bcrypt \
        py3-cffi \
        py3-six \
        py3-pip && \
    adduser -S -u 1001 -G root pypiserver && \
    mkdir -p /data/packages && \
    chown -R pypiserver:root /data/packages && \
    chmod -R g+s /data/packages && \
    chgrp 0 /etc/passwd && \
    chmod g=u /etc/passwd

# Install pypiserver
RUN pip3 install --no-cache-dir passlib==1.7.2 pypiserver==${version}

COPY --chown=1001:0 uid_entrypoint.sh /usr/local/bin/
COPY --chown=1001:0 .htpasswd /data/auth/

USER 1001

WORKDIR /data

EXPOSE 8080

ENTRYPOINT ["/usr/local/bin/uid_entrypoint.sh"]

CMD ["/usr/bin/pypi-server", "-v", "-p", "8080", "-P", "/data/auth/.htpasswd", "-a", "update,download,list", "/data/packages"]

set -e

export MAJOR_VERSION=$(echo "$@" | sed -re 's/^([0-9]+)\.([0-9]+)\..+/\1/g')
export MINOR_VERSION=$(echo "$@" | sed -re 's/^([0-9]+\.[0-9]+)\..+/\1/g')

docker pull $CI_REGISTRY_IMAGE:dev || true
docker build --cache-from $CI_REGISTRY_IMAGE:dev --build-arg version=$@ --tag $CI_REGISTRY_IMAGE:$@ --tag $CI_REGISTRY_IMAGE:$MINOR_VERSION --tag $CI_REGISTRY_IMAGE:$MAJOR_VERSION --tag $CI_REGISTRY_IMAGE:latest .
docker push $CI_REGISTRY_IMAGE:$@
docker push $CI_REGISTRY_IMAGE:$MINOR_VERSION
docker push $CI_REGISTRY_IMAGE:$MAJOR_VERSION
docker push $CI_REGISTRY_IMAGE:latest
